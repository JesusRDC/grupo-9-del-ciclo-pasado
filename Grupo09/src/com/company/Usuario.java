package com.company;

public abstract class Usuario {
    private String Nombre;
    private String Apellido;
    private String Correo;

    public Usuario(String nombre, String apellido, String correo) {
        Nombre = nombre;
        Apellido = apellido;
        Correo = correo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getApellido() {
        return Apellido;
    }

    public void setApellido(String apellido) {
        Apellido = apellido;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public abstract void Mostrar();
}
