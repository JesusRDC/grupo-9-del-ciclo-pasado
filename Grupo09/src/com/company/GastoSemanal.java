package com.company;

public class GastoSemanal extends Reporte_Gastos{
    private double CantGasto;

    public GastoSemanal(Integer fecha, double cantGasto) {
        super(fecha);
        CantGasto = cantGasto;
    }
    public double getCantGasto() {
        return CantGasto;
    }

    public void setCantGasto(double cantGasto) {
        CantGasto = cantGasto;
    }

    public void MostrarGastos(){
        System.out.println("Gasto Semanal: "+CantGasto);
    }
}
