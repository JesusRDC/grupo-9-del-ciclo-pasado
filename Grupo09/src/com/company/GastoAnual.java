package com.company;

public class GastoAnual extends Reporte_Gastos{
    private double CantGasto;

    public GastoAnual(Integer fecha, double cantGasto) {
        super(fecha);
        CantGasto = cantGasto;
    }

    public double getCantGasto() {
        return CantGasto;
    }

    public void setCantGasto(double cantGasto) {
        CantGasto = cantGasto;
    }
    public void MostrarGastos(){
        System.out.println("Gasto Anual: "+CantGasto);
    }
}
