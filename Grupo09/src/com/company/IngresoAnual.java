package com.company;

public class IngresoAnual extends Reporte_Ingresos{
    private double CantIngresos;

    public IngresoAnual(Integer fecha, double cantIngresos) {
        super(fecha);
        CantIngresos = cantIngresos;
    }

    public void MostrarIngreso(){
        System.out.println("Ingreso Anual: "+CantIngresos);
    }
}
