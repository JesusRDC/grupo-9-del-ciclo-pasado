package com.company;

public class Ejecutivo extends Usuario {
    private String Cargo;

    public Ejecutivo(String nombre, String apellido, String correo, String cargo) {
        super(nombre, apellido, correo);
        Cargo = cargo;
    }
    public void Mostrar(){
        System.out.println("Nombre: "+getNombre());
        System.out.println("Apellido: "+getApellido());
        System.out.println("Correo: "+getCorreo());
        System.out.println("Cargo: "+Cargo);

    }
}
