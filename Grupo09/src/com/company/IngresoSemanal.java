package com.company;

public class IngresoSemanal extends Reporte_Ingresos{
    private double CantIngresos;

    public IngresoSemanal(Integer fecha, double cantIngresos) {
        super(fecha);
        CantIngresos = cantIngresos;
    }

    public void MostrarIngreso(){
        System.out.println("Ingreso Semanal: "+CantIngresos);
    }
}
