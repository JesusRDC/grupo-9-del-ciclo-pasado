package com.company;

public class Trabajador extends Usuario {
    private double salario;


    public Trabajador(String nombre, String apellido, String correo, double salario) {
        super(nombre, apellido, correo);
        this.salario = salario;
    }

    public void Mostrar(){
        System.out.println("Nombre: "+getNombre());
        System.out.println("Apellido: "+getApellido());
        System.out.println("Correo: "+getCorreo());
        System.out.println("Salario: "+salario);

    }

}
