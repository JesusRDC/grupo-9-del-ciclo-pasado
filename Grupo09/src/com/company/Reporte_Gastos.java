package com.company;

public abstract class Reporte_Gastos {
    private Integer fecha;

    public Reporte_Gastos(Integer fecha) {
        this.fecha = fecha;
    }
    public  abstract void MostrarGastos();
}
