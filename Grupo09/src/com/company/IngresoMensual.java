package com.company;

public class IngresoMensual extends Reporte_Ingresos{
    private double CantIngresos;

    public IngresoMensual(Integer fecha, double cantIngresos) {
        super(fecha);
        CantIngresos = cantIngresos;
    }

    public void MostrarIngreso(){
        System.out.println("Ingreso Mensual: "+CantIngresos);
    }
}
